@extends('app')

@section('title','FIND')


@section('css')
    
    <style>
        .menu-main li a{
            color:black !important;
        }
    </style>

@endsection

@section('main')


    <find-component :id="{{request()->segment(2)}}" />

@endsection
