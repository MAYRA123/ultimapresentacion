require('./bootstrap');

// PAGES COMPONENT

import Registro from './components/RegistroComponent';
import Listado from './components/ListadoComponent';
import Find from './components/FindComponent';

Vue.component('registro-component',Registro);
Vue.component('listado-component',Listado);
Vue.component('find-component',Find);

let app = new Vue({
    
    el:'#app',
    data:{

    },
    methods:{

    },
    async mounted(){


        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });


        
    }
});