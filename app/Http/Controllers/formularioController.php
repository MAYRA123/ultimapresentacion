<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\formularioModel;
use Illuminate\Support\Facades\Validator;

class formularioController extends Controller
{
    public function create(Request $request){
        Validator::make($request->all(),[
            'carnet'=>'required',
            'gestion'=>'required',
            'matricula'=>'required',
            'carrera'=>'required',
            'nivel'=>'required',
            'grupo'=>'required',
            'paterno'=>'required',
            'materno'=>'required',
            'nombres'=>'required',
            'nacimiento'=>'required|date',
            'calle'=>'required',
            'zona'=>'required',
            'telefono'=>'required',
            'apoderado'=>'required',
            'estado'=>'required',
            'genero'=>'required'
        ])->validate();

        $carnet = $request->post('carnet');
        $gestion = $request->post('gestion');
        $matricula = $request->post('matricula');
        $carrera = $request->post('carrera');
        $nivel = $request->post('nivel');
        $grupo = $request->post('grupo');
        $paterno = $request->post('paterno');
        $materno = $request->post('materno');
        $nombres = $request->post('nombres');
        $nacimiento = $request->post('nacimiento');
        $calle = $request->post('calle');
        $zona = $request->post('zona');
        $telefono = $request->post('telefono');
        $apoderado = $request->post('apoderado');
        $estado = $request->post('estado');
        $genero = $request->post('genero');


         formularioModel::create([
             'carnet'=>$carnet,
             'gestion'=>$gestion,
             'matricula'=>$matricula,
             'carrera'=>$carrera,
             'nivel'=>$nivel,
             'grupo'=>$grupo,
             'apellido_materno'=>$materno,
             'apellido_paterno'=>$paterno,
             'nombres'=>$nombres,
             'nacimiento'=>$nacimiento,
             'calle'=>$calle,
             'zona'=>$zona,
             'telefono'=>$telefono,
             'nombres_apoderado'=>$apoderado,
             'estado'=>$estado,
             'genero'=>$genero
         ]);

         
        return response()->Json([
            'message'=>'Se Creo Correctamente'
        ],200);
        return $request->all();

    }




}
