<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\guestController;
use App\Http\Controllers\formularioController;


Route::get('/',[guestController::class,'index'])->name('init');



//Auth::routes();//todas las rutas de autentificacion

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('/{any}',[App\Http\Controllers\HomeController::class, 'index'])->where('any','.*');

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::middleware(['auth:web'])->group(function(){
    Route::post('create',[formularioController::class,'create'])->name('create');
    Route::get('list',[HomeController::class,'listado'])->name('listado');
    Route::get('listado/{formulario:id}',[HomeController::class,'listadoFind'])->name('formulario.find');
    Route::get('salir',[HomeController::class,'salir'])->name('salir');

    Route::get('list/usuarios/final',[HomeController::class,'listarUsuarios'])->name('listado.final');
    Route::get('usuario/find/{formulario:id}',[HomeController::class,'listadoApiFind'])->name('formulario.find');

});